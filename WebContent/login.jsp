<%@page import="edu.uclm.esi.tysweb.laoca.dominio.Manager"%>
<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.laoca.dominio.*"%>
<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
	String p=request.getParameter("p");
	JSONObject jso=new JSONObject(p);
	
	JSONObject respuesta=new JSONObject();
	try {
		String email=jso.getString("email");
		String pwd=jso.getString("pwd");
		String tipoDeBroker=jso.getString("tipoDeBroker");
		Usuario usuario=Manager.get().login(email, pwd, tipoDeBroker);
		session.setAttribute("usuario", usuario);
		respuesta.put("result", "OK");
		respuesta.put("mensaje", email + " conectado");
	}
	catch (Exception e) {
		respuesta.put("result", "ERROR");
		respuesta.put("mensaje", e.getMessage());
	}
	out.println(respuesta.toString());
%>