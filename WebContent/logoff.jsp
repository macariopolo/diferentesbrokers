<%@page import="edu.uclm.esi.tysweb.laoca.dominio.Manager"%>
<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.laoca.dominio.Usuario"%>
<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
	JSONObject respuesta=new JSONObject();
	try {
		Usuario usuario=(Usuario) session.getAttribute("usuario");
		Manager.get().logoff(usuario.getLogin());
		respuesta.put("result", "OK");
		respuesta.put("mensaje", usuario.getLogin() + " desconectado");
	}
	catch (Exception e) {
		respuesta.put("result", "ERROR");
		respuesta.put("mensaje", e.getMessage());
	}
	out.println(respuesta.toString());
%>