package edu.uclm.esi.tysweb.laoca.dominio;

import java.util.concurrent.ConcurrentHashMap;

public class Manager {
	private ConcurrentHashMap<String, Usuario> usuarios;
	
	private Manager() {
		this.usuarios=new ConcurrentHashMap<>();
	}
	
	public Usuario login(String email, String pwd, String tipoDeBroker) throws Exception {
		Usuario usuario=new Usuario(email);
		if (tipoDeBroker.equals("conPool")) {
			if (!usuario.existeConPool(pwd))
				throw new Exception("Usuario o contraseña inválidos");
		} else if (tipoDeBroker.equals("abriendoYCerrandoConexion")) {
			if (!usuario.existeAbriendoYCerrando(pwd))
				throw new Exception("Usuario o contraseña inválidos");
		} else 
			throw new Exception("Broker desconocido");
		this.usuarios.put(email, usuario);
		return usuario;
	}
	
	public void logoff(String email) {
		this.usuarios.remove(email);
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}
	
}
